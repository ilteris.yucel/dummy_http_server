from http.server import HTTPServer
from req_handler import RequestHandler


class app():
    def __init__(self):
        self.ip = ''
        self.port = 8000
        self.handler = RequestHandler
        self.server_class = HTTPServer

    def run(self, server_class=None, ip=None, port=None, handler=None):
        if server_class is not None:
            self.server_class = server_class
        if ip is not None:
            self.ip = ip
        if port is not None:
            self.port = port
        if handler is not None:
            self.handler = handler
        server_address = (self.ip, self.port)
        httpd = self.server_class(server_address, self.handler)
        httpd.serve_forever()

    def add_get_rule(self, path, func):
        self.handler.rule_dict[path] = func


if __name__ == '__main__':
    server = app()

    def get_handler():
        return "Hello World", 200
    server.add_get_rule("/hello", get_handler)
    print("Server on port : {}".format(server.port))
    server.run()
