from http.server import BaseHTTPRequestHandler
import json


class RequestHandler(BaseHTTPRequestHandler):
    rule_dict = {}

    def set_headers(self, status_code=200, header_dict=None):
        if header_dict is None:
            header_dict = {"Content-type": "application/json"}
        self.send_response(status_code)
        for k, v in header_dict.items():
            self.send_header(k, v)
        self.end_headers()

    def do_GET(self, message=None, status_code=200, header_dict=None):
        if message is None:
            message = {"status": "success", "message": "OK"}
        if self.path in self.rule_dict.keys():
            message, status_code = self.rule_dict[self.path]()
        print("Request path  is {}".format(self.path))
        # print("Request : {}".format(self.request.recv(1024).strip()))
        self.set_headers(status_code, header_dict)
        self.wfile.write(bytes(json.dumps(message), encoding="utf-8"))






